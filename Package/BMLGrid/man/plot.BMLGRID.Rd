\name{summary.BMLGRID}
\title{
Plot of a S3 BMLGRID class Object
}
\description{
The function is designed to plot a S3 BMLGRID class Object
}
\usage{
plot.BMLGRID(g)
}
\arguments{
\item{g}{
An GMLGRID S3 class object
}
\value{
Plot of GMLGRID
}
}
\author{
Linfeng Li
}
\examples{
grid <- createBMLGrid(r=4,c=4,ncars=c(red=5,blue=5))
plot(grid)
}
